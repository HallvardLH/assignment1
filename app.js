const totalPay = document.getElementById("totalPay")
const bankButton = document.getElementById("bankButton")
const workButton = document.getElementById("workButton")
const repayLoanButton = document.getElementById("repayLoanButton")
const totalBalance = document.getElementById("totalBalance")
const getLoanButton = document.getElementById("getLoanButton")
const outstandingLoan = document.getElementById("outstandingLoan")
const outstandingLoanText = document.getElementById("outstandingLoanText")
const selectLaptopsElement = document.getElementById("selectLaptopsElement")
const laptopFeaturesElement = document.getElementById("laptopFeaturesElement")
const laptopDescriptionElement = document.getElementById("laptopDescriptionElement")
const laptopImageElement = document.getElementById("laptopImageElement")
const buyPcButton = document.getElementById("buyPcButton")
const laptopTitleElement = document.getElementById("laptopTitleElement")
const laptopPriceElement = document.getElementById("laptopPriceElement")
const laptopCurrencyElement = document.getElementById("laptopCurrencyElement")
const displayLaptopsDiv = document.getElementById("displayingLaptops")

// Array to store laptops fetched from API
let laptops = []
// Variable keeps track of wether the user has bought a laptop or 
// not so that he can apply for another loan.
let canTakeLoan = true

// Get JSON-data about laptops and populate select-element
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => { // Return json-data as javascript object
        return response.json()
    })
    .then(data => { // Pass the parsed laptops to method
        laptops = data
        addLaptopsToMenu(laptops)
    })
    .catch(error => {
        console.error(error);
    })
    

// Method populates the select element
const addLaptopsToMenu = (laptops) => {
    laptops.forEach(laptop => {
        const laptopElement = document.createElement("option")
        laptopElement.value = laptop.title
        // Make selector display laptop.title
        laptopElement.appendChild(document.createTextNode(laptop.title))
        selectLaptopsElement.appendChild(laptopElement)
    });
}

// Click on workButton increases pay by 100
workButton.addEventListener("click", function(){
    pay = parseInt(totalPay.innerText) + 100
    totalPay.innerText = pay
})

// Click on getLoanButton
getLoanButton.addEventListener("click", function(){
    // If user has not bought a computer he can't take another loan
    if (!canTakeLoan){
        alert("You must buy a computer before you can take another loan!")
        return
    }

    // If there already is an outstanding loan, quit function
    if (parseInt(outstandingLoan.innerText) > 0){
        alert("You already have an outstanding loan!")
        return
    }

    loanAmount = prompt("Please choose how much you would like to loan")
    maxLoanAmount = parseInt(totalBalance.innerText) * 2
    // Loan amount can not exceed twice the balance
    if (loanAmount > maxLoanAmount) {
        alert(`Your loan can not exceed ${maxLoanAmount}`)
        return
    }  

    // Check for valid input. User cannot return a negative number,
    // non-numeric string, empty string or null
    if (parseInt(loanAmount) <= 0 || !Boolean(loanAmount) || isNaN(loanAmount) || loanAmount.toString().indexOf(' ') >= 0) {
        alert("Enter a valid sum")
        return
    }  
    // Make outstanding loan and repay loan button visible
    outstandingLoanText.removeAttribute("hidden")
    repayLoanButton.removeAttribute("hidden")
    // Update outstanding loan
    outstandingLoan.innerText = loanAmount
    // User must buy a computer before taking another loan
    canTakeLoan = false   
})

// Click on laptop selector shall update features and price
selectLaptopsElement.addEventListener("change", function(){
    // Get index of selected laptop
    index = selectLaptopsElement.selectedIndex

    // Structure the specs
    specsFormatted = ""
    for (let i = 0; i < laptops[index].specs.length; i++) {
        specsFormatted += `${laptops[index].specs[i]}\n`
    }
    // Set specs in the features tag
    laptopFeaturesElement.innerText = specsFormatted

    // Update laptop titles
    laptopTitleElement.innerText = laptops[index].title

    // Update laptop description
    laptopDescriptionElement.innerText = laptops[index].description

    // Update laptop image     
    pcImg = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[index].image
    laptopImageElement.src = pcImg

    // Update laptop price
    laptopPriceElement.innerText = laptops[index].price
    laptopCurrencyElement.removeAttribute("hidden")

    displayLaptopsDiv.removeAttribute("hidden")
})

// Click on bank button
bankButton.addEventListener("click", () => {
    balance = parseInt(totalBalance.innerText)
    // If there is no outstanding loan, transfer the entire pay to balance
    if (parseInt(outstandingLoan.innerText) == 0){
        balance += parseInt(totalPay.innerText)
    }
    // If there is an outstanding loan, use 10% of pay to pay back loan.
    // Rest is transfered to balance
    else{
        loanPayBack = parseInt(totalPay.innerText) * 0.1
        // If loanPayBack is larger than outstanding loan we only transfer whats possible
        if (loanPayBack > parseInt(outstandingLoan.innerText)){
            balance += loanPayBack - parseInt(outstandingLoan.innerText)
            loanPayBack = parseInt(outstandingLoan.innerText)
        }
        payToBalance = parseInt(totalPay.innerText) * 0.9
        outstandingLoan.innerText = parseInt(outstandingLoan.innerText) - loanPayBack
        balance += payToBalance
        // Set button as hidden if outstandingLoan is 0
        if (parseInt(outstandingLoan.innerText) == 0){
            repayLoanButton.setAttribute("hidden", true )
        }
    }
    totalBalance.innerText = balance.toString()
    totalPay.innerText = "0"
})

// Repay loan button is only visible once there is a loan.
// When clicked, all of the pay goes towards downpayment of loan
repayLoanButton.addEventListener("click", () => {
    balance = parseInt(totalBalance.innerText)
    loan = parseInt(outstandingLoan.innerText)
    pay = parseInt(totalPay.innerText)
    toBalance = pay - loan

    // If toBalance is positive this means that pay is larger than loan.
    // Thus, we will transfer the remainder to the balance
    if (toBalance >= 0) {
        totalBalance.innerText = (balance + toBalance).toString()
        outstandingLoan.innerText = "0"
        totalPay.innerText = "0"
        repayLoanButton.setAttribute("hidden", true)
    }
    else{ // If toBalance is negative, all of the pay goes towards downpayment
        outstandingLoan.innerText = loan - pay
        totalPay.innerText = "0"
    }
})

// When clicked user attempts to buy selected pc. If user cannot afford,
// message is shown.
buyPcButton.addEventListener("click", () =>{
    balance = parseInt(totalBalance.innerText)
    pcPrice = parseInt(laptopPriceElement.innerText)
    pcTitle = laptops[selectLaptopsElement.selectedIndex].title
    console.log(balance, pcPrice, pcTitle);

    // User has sufficient balance
    if (balance >= pcPrice){
        alert(`You are now the owner of ${pcTitle}`)
        totalBalance.innerText = (balance - pcPrice).toString()
        canTakeLoan = true
    }
    else{ // User can't afford selected pc
        alert("You don't have enough money to buy this pc")
    }
})